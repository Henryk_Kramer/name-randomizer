Author: Henryk Kramer
isNumeric-Algorithm: Charles Salvia 'https://stackoverflow.com/users/168288/charles-salvia'
Created: December 2018

This program returns a random name from a txt file. You can also specify if you do not want names to be drawn.