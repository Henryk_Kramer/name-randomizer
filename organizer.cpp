#include "organizer.h"

//logging and getting
#include <iostream>
#define LOG(x) std::cout << x << std::endl

//libraries
#include <cctype>
#include <algorithm>
#include <sstream>
#include "readerWriter.h"
#include "randomizer.h"


//constructor

Organizer::Organizer(const std::string& path) : path(path) {

    LOG("File with the names: '" << path << "'\n");
    arrayList = fileToArraylist(path);

}


//main method

//run the main-code
void Organizer::run() {

    
    if(arrayList -> getSize() == 0) {
        std::cout << "The file is empty. With no information the program cannot progress." << std::endl;
        return;
    };

    std::cout << "---- Names -----" << std::endl;
    logArraylist();
    std::cout << "----------------" << std::endl;

    LOG("\nPlease enter the names(number) of those not present seperated with a whitespace character!\n");
    removeNamesFromArraylist();
    LOG("\nTap 'Enter' to draw again!\n");

    do {
        std::cout << ">> Chosen: " << drawName();
        std::getchar();
    } while (true);
    
    //no need to delete arrayList, for the program does not continue after the code above
}

//secondary methods

//print out all the names from the given path with an index
void Organizer::logArraylist() {

    uint64_t size = arrayList -> getSize();
    for(int i = 0; i < size; i++) {
        LOG(i << ". " << arrayList -> get(i));
    }

}

//read in the indices and deletes them from the arraylist
//output the deleted names
void Organizer::removeNamesFromArraylist() {

    ArrayList<uint32_t> tmpArrlist;

    std::string s;
    std::string line;
    bool mixedInput = true;

    while(mixedInput){
        std::cout << "User: ";
        std::getline(std::cin, line);
        std::stringstream ss(line);

        tmpArrlist.clearAll();
        mixedInput = false;

        while(ss >> s){
            if(isStringNumeric(s)) {
                tmpArrlist.add(std::stoi(s));
            } else {
                mixedInput = true;
            }
        }

    }
       
    std::sort(tmpArrlist.begin(), tmpArrlist.end());
    
    if(tmpArrlist.getSize() != 0) std::cout << "\n";
    for(uint32_t i = 0, len = tmpArrlist.getSize(); i < len; i++) {
        if(tmpArrlist.get(i)-i < arrayList -> getSize()) {
            std::cout << ">> Taken out: " << arrayList -> remove(tmpArrlist.get(i)-i) << std::endl;
        }
    }
    
}

//return a random name from the file
std::string Organizer::drawName() {

    if(!arrayList -> isEmpty()) {
        uint64_t randInt = randomByTime(arrayList -> getSize());
        std::string name = arrayList -> remove(randInt);
        deleteLine(path.c_str(), randInt);
        return name;
    }
    return "None";

}

//helpers

//return if a string is numeric
inline bool Organizer::isStringNumeric(const std::string& s) { //algorithm by Charles Salvia from stackoverflow 'https://stackoverflow.com/users/168288/charles-salvia'

    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();

} 