#pragma once

#include <iostream>

//libraries
#include <ostream>
#include "arrayList_structure.h"


//helper methods

//shif the data from one array to another with changed length
//then shift back
template <typename T>
void ArrayList<T>::shiftData() {
    T* tmp = new T[length];
    T* tmpDelete = array;

    for(uint32_t i = 0; i < size; i++) {
        tmp[i] = array[i];
    }

    array = tmp;
    delete [] tmpDelete;
}

//move up the elements in the array (better used for adding one element)
template <typename T>
void ArrayList<T>::moveUp(uint64 index) {

    resize();
    for(uint32_t i = size-1; i >= index; i--) {
        array[i+1] = array[i];
    }

}

//move up the elements in the array (better used for adding more elements)
template <typename T>
void ArrayList<T>::moveUpWithoutResize(uint64 index) {

    for(uint32_t i = size-1; i >= index; i--) {
        array[i+1] = array[i];
    }

}

//move back the elements in the array
template <typename T>
void ArrayList<T>::moveDown(uint64 index) {

    for(uint32_t i = index; i < size-1; i++) {
        array[i] = array[i+1];
    }

}


//Constructor/Destructor

template <typename T>
ArrayList<T>::ArrayList(uint64 startLength, bool plus, uint64 added) {
    this -> plus = plus;
    this -> added = added;
    size = 0;
    length = startLength;
    array = new T[length];
}

template <typename T>
ArrayList<T>::~ArrayList() {
    delete [] array;
}


//tools


//resize the array so the length fits the size of the elements (better used for adding one element)
template <typename T>
void ArrayList<T>::fit() {
    if(size != length) {
        length = size;

        shiftData();
    }
}

//resize the array with use of the parameters handed over in the constructor (better used for adding more elements)
template <typename T>
void ArrayList<T>::resize() {
    if(size == length) {
        length = plus ? length + added : length * added;

        shiftData();
    }
}

//resize the array by parameter if needed to
template <typename T>
void ArrayList<T>::resize(uint32_t addSize) {
    if(size + addSize > length) {
        length = size + addSize;
        shiftData();
    }
}

//prepare array for reuse
template <typename T>
void ArrayList<T>::clearAll() {
    length = 0;
    shiftData();
}

//check if there are no elements in the array
template <typename T>
bool ArrayList<T>::isEmpty() {
    return (size == 0) ? true : false;
}

//return the number of elements in the array
template <typename T>
uint64 ArrayList<T>::getSize() const {
    return size;
}

//return the length of the array
template <typename T>
uint64 ArrayList<T>::getLength() const {
    return length;
}

//return the first element of the array as pointer
template <typename T>
T* ArrayList<T>::begin() {
    return &array[0];
}

//return the last element of the array as pointer
template <typename T>
T* ArrayList<T>::end() {
    return &array[size];
}


//setter / writer

//add one element to the end of the array
template <typename T>
void ArrayList<T>::add(const T& t) {
    resize();
    array[size] = t;
    size++;
}

//add one element at a specific position in the array
template <typename T>
void ArrayList<T>::addAt(uint64 index, const T& t) {
    if(index < size) {
        moveUp(index);
        array[index] = t;
        size++;
    } else {
        throw std::invalid_argument("Index groesser als Elemente in der Liste vorhanden");
    }
}

//add an array of elements to the array
template <typename T>
void ArrayList<T>::add(T *t, uint64 addSize) {
    resize(addSize);
    for(uint32_t i = 0; i < addSize; i++) {
        array[i] = t[i];
        size++;
    }
}

//add an array of elements at a specific position to the array
template <typename T>
void ArrayList<T>::addAt(uint64 index, T *t, uint64 addSize) {
    if(index < size) {
        resize(addSize);

        for(uint32_t i = 0; i < addSize; i++) {
            moveUpWithoutResize(index + i);
            size++;
        }

        for(uint32_t i = 0; i < addSize; i++) {
            array[index + i] = t[i];
        }
    } else {
        throw std::invalid_argument("Index groesser als Elemente in der Liste vorhanden");
    }
}

//replace an element with another at a specific position in the array
template <typename T>
void ArrayList<T>::set(uint64 index, const T& t) {
    if(index < size) {
        array[index] = t;
    } else {
        throw std::invalid_argument("Index groesser als Elemente in der Liste vorhanden");
    }
}

//replace a bunch of elements with an array of others at a specific position in the array
template <typename T>
void ArrayList<T>::set(uint64 index, T *t, uint64 addSize) {
    if(index + addSize < size) {
        for(uint32_t i = 0; i < addSize; i++) {
            array[index + i] = t[i];
        }
    } else {
        throw std::invalid_argument("Index groesser als Elemente in der Liste vorhanden");
    }
}

//remove the last element from the array
template <typename T>
T ArrayList<T>::remove() {
    T t = array[size-1];
    array[size-1] = 0;
    size--;
    return t;
}

//remove an element at a specific position from the array
template <typename T>
T ArrayList<T>::remove(uint64 index) {
    if(index < size && size > 0) {
        T t = array[index];
        moveDown(index);
        size--;
        return t;
    } else {
        throw std::invalid_argument("Index groesser als Elemente in der Liste vorhanden");
    }
}


//getter

//return the element at a specific position
template <typename T>
T& ArrayList<T>::get(uint64 index) const {
    if(index < size) {
        return array[index];
    }
    throw std::invalid_argument("Index groesser als Elemente in der Liste vorhanden");
}

//return the index of a specific element (only works for primitive datatypes)
template <typename T>
int32_t ArrayList<T>::indexOf(const T& t) const {
    if(std::is_fundamental<T>::value) {
        for(uint32_t i = 0; i < size; i++) {
            if(array[i] == t) {
                return i;
            }
        }
        return -1;
    }
    return -2;
}


//overloaded operators

//return the element at a specific position
template <typename T>
T& ArrayList<T>::operator[](uint64 index) {
    return get(index);
}

//add one element to the end of the array
template <typename T>
void ArrayList<T>::operator+(const T& t) {
    return add(t);
}

//remove the last element from the array
template <typename T>
T ArrayList<T>::operator--(int) {
    return remove();
}

//remove the first element from the array
template <typename T>
T ArrayList<T>::operator--() {
    return remove(0);
}
