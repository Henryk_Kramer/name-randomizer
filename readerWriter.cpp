#include <iostream>

//libraries
#include <algorithm>
#include <fstream>
#include "readerWriter.h"


//read in from a file and return the input as arraylist
ArrayList<std::string>* fileToArraylist(const std::string& path) {

    ArrayList<std::string> *arr = new ArrayList<std::string>(16, false, 2);

    std::ifstream file(path);
    std::string s;

    while(std::getline(file, s)){
        arr -> add(s);
    }
    file.close();
    
    arr -> fit();

    return arr;
    
}


//delete a line from the given file
void deleteLine(const char* path, uint32_t index) {

    std::ifstream inFile(path);
    std::ofstream outFile("tmp.txt");
    std::string s;
    uint64_t lineNum = 0;

    while(std::getline(inFile, s)){
        if(lineNum != index) {
            outFile << s << std::endl;
        }
        lineNum++;
    }
    inFile.close();
    outFile.close();

    std::remove(path);
    std::rename("tmp.txt", path);

}

//delete an array of lines from the given file
void deleteLine(const char* path, uint32_t* indices, uint32_t arrSize) {

    std::sort(indices, indices + arrSize);

    std::ifstream iFile(path);
    std::ofstream oFile("tmp.txt");
    std::string s;
    uint32_t lineNum = 0;
    uint32_t index = 0;

    while(std::getline(iFile, s)){
        if(lineNum != indices[index]) {
            oFile << s << std::endl;
        } else {
            index++;
        }
        lineNum++;
    }
    iFile.close();
    oFile.close();

    std::remove(path);
    std::rename("tmp.txt", path);

}