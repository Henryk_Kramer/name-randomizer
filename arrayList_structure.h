#pragma once

//typedef
typedef unsigned long long uint64;


//class template
template <typename T>
class ArrayList
{
    private:
        //data
        T* array;
        uint64 size;
        uint64 length;

        //modifier
        bool plus;
        uint64 added;

        //helpers
        void shiftData();
        void moveUp(uint64 index);
        void moveUpWithoutResize(uint64 index);
        void moveDown(uint64 index);

    public:
        ArrayList(uint64 startLength = 8, bool adder = true, uint64 added = 8);
        ~ArrayList();

        //tools
        void fit();
        void resize();
        void resize(uint32_t addSize);
        void clearAll();
        bool isEmpty();
        T* begin();
        T* end();
        uint64 getSize() const;
        uint64 getLength() const;

        //setter / writer
        void add(const T& t);
        void addAt(uint64 index, const T& t);
        void add(T *t, uint64 size);
        void addAt(uint64 index, T *t, uint64 addSize);
        void set(uint64 index, const T& t);
        void set(uint64 index, T *t, uint64 size);
        T remove();
        T remove(uint64 index);

        //getter
        T& get(uint64 index) const;
        int32_t indexOf(const T& t) const;

        //overloaded operators
        T& operator[](uint64 index);
        void operator+(const T& t);
        T operator--(int);
        T operator--();
};