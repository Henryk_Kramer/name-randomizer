//COMPILE: g++ randomizer.cpp readerWriter.cpp organizer.cpp main.cpp -o Randomizer.exe
//START: start Randomizer.exe

#include <iostream>

#include "organizer.h"

main(int argc, char const *argv[])
{   
    Organizer orga ("names.txt");
    orga.run();

    std::cout << "\n";
    system("pause");
}