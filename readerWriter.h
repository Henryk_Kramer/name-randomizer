#pragma once

//libraries
#include <string>
#include "arrayList.h"


//functions
ArrayList<std::string>* fileToArraylist(const std::string& path);
void deleteLine(const char* path, uint32_t index);
void deleteLine(const char* path, uint32_t* indices, uint32_t arrSize);