#include "randomizer.h"

//libraries
#include <chrono>

//namespaces
using namespace std::chrono;


//return a random number using the time
uint64 randomByTime(uint64 amount) {
    return (duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count()) % amount;
}