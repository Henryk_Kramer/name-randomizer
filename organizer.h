#pragma once

#include <string>
#include "arrayList.h"

class Organizer {
    private:
        ArrayList<std::string>* arrayList;
        std::string path;

    public:
        Organizer(const std::string& path);

        //main methods
        void run();

        //secondary methods
        void logArraylist();
        void removeNamesFromArraylist();
        std::string drawName();

        //helpers
        bool isStringNumeric(const std::string&);
};